import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:taxi/contact/contact_page.dart';

class OTPScreen extends StatefulWidget {
  String phone;
  String? countrycode;
  String verificationID;
  OTPScreen(
      {required this.phone,
      required this.countrycode,
      required this.verificationID});

  @override
  State<OTPScreen> createState() => _OTPScreenState();
}

class _OTPScreenState extends State<OTPScreen> {
  final GlobalKey<ScaffoldState> _scaffoldkey = GlobalKey<ScaffoldState>();
  FirebaseAuth _auth = FirebaseAuth.instance;
  String otpcode = "";
  TextEditingController _otpfield1 = TextEditingController();
  TextEditingController _otpfield2 = TextEditingController();
  TextEditingController _otpfield3 = TextEditingController();
  TextEditingController _otpfield4 = TextEditingController();
  TextEditingController _otpfield5 = TextEditingController();
  TextEditingController _otpfield6 = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldkey,
      appBar: AppBar(
        automaticallyImplyLeading: true,
        toolbarHeight: 150,
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Container(
            child: Image.asset('images/ic_back.png'),
          ),
        ),
        bottom: PreferredSize(
          preferredSize: Size.fromHeight(20),
          child: Container(
            margin: EdgeInsets.fromLTRB(20, 0, 0, 35),
            alignment: Alignment.bottomLeft,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                    child: Text(
                      'Phone Verification',
                      style: TextStyle(fontSize: 26, color: Colors.white),
                    )),
                Container(
                    margin: EdgeInsets.only(top: 10),
                    child: Text(
                      'Enter your OTP Code here',
                      style: TextStyle(fontSize: 16, color: Colors.white),
                    ))
              ],
            ),
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.only(top: 35),
          alignment: Alignment.center,
          child: Column(
            children: [
              Text(
                'Enter the 4 digit code we just sent to',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
              ),
              SizedBox(
                height: 5,
              ),
              Text(
                '${widget.countrycode}${widget.phone}',
                style: TextStyle(
                    color: Color(0xFF109F2E),
                    fontSize: 16,
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 40,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  _otpfield(_otpfield1),
                  _otpfield(_otpfield2),
                  _otpfield(_otpfield3),
                  _otpfield(_otpfield4),
                  _otpfield(_otpfield5),
                  _otpfield(_otpfield6),
                ],
              ),
              SizedBox(
                height: 30,
              ),
              GestureDetector(
                onTap: () async {
                  setState(() {
                    otpcode = _otpfield1.text +
                        _otpfield2.text +
                        _otpfield3.text +
                        _otpfield4.text +
                        _otpfield5.text +
                        _otpfield6.text;
                  });
                  PhoneAuthCredential phoneAuthCredential = PhoneAuthProvider.credential(
                      verificationId: widget.verificationID, smsCode: otpcode);
                  signInWithPhoneAuth(phoneAuthCredential);
                },
                child: Container(
                  alignment: Alignment.center,
                  padding: EdgeInsets.symmetric(vertical: 15),
                  margin: EdgeInsets.symmetric(horizontal: 40),
                  decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: Color(0xFF51C569),
                          blurRadius: 15.0,
                          offset: Offset(2.0, 2.0),
                        )
                      ],
                      borderRadius: BorderRadius.circular(10),
                      gradient: LinearGradient(
                          begin: Alignment.centerRight,
                          end: Alignment.centerLeft,
                          colors: [
                            Color(0xFF51C569),
                            Color(0xFF109F2E),
                          ])),
                  child: Text(
                    'SIGN IN',
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _otpfield(TextEditingController controller) {
    return Container(
      height: 50,
      width: 50,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(8),
          border: Border.all(
            color: Color(0xFF34495E),
          )),
      child: TextField(
        onChanged: (value) {
          if (value.length == 1) {
            FocusScope.of(context).nextFocus();
          }
        },
        controller: controller,
        inputFormatters: [
          FilteringTextInputFormatter.digitsOnly,
          LengthLimitingTextInputFormatter(1),
        ],
        keyboardType: TextInputType.number,
        maxLines: 1,
        decoration: InputDecoration(
          border: InputBorder.none,
        ),
        textAlign: TextAlign.center,
      ),
    );
  }

  void signInWithPhoneAuth(PhoneAuthCredential phoneAuthCredential) async {
    try {
      final authCredential = await _auth.signInWithCredential(phoneAuthCredential);
      if(authCredential.user != null){
        Navigator.push(context, MaterialPageRoute(builder: (context) => ContactScreen()));
      }
    } on Exception catch (e) {
      // TODO
    }
  }
}
