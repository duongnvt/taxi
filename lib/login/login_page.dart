import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:taxi/login/login_cubit.dart';
import 'package:taxi/login/sign_in.dart';
import 'package:taxi/login/sign_up.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) {
        return LoginCubit();
      },
      child: LoginScreen(),
    );
  }
}

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  LoginCubit? _cubit;

  @override
  void initState() {
    _cubit = context.read<LoginCubit>();
    super.initState();
    _cubit?.Page();
  }

  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
      child: Container(
        height: MediaQuery.of(context).size.height,
        child: Stack(
          children: [
            Column(
              children: [
                Expanded(
                    child: Container(
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          colors: [
                        Color(0xFF51C569),
                        Color(0xFF109F2E),
                      ])),
                )),
                Expanded(
                    child: Container(
                  color: Colors.white,
                ))
              ],
            ),
            Container(
              alignment: Alignment.center,
              child: BlocBuilder<LoginCubit, LoginState>(builder: (context, state) {
                return Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Container(
                        alignment: Alignment.center,
                        child: Image.asset('images/taxi_logo.png')),
                    SizedBox(height: 50,),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 20),
                      height: 320,
                      decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                              color: Color(0xFF51C569),
                              blurRadius: 20.0,
                              offset: Offset(2.0, 2.0),
                            )
                          ],
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10)),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              buildRow("Sign Up", () {
                                _cubit?.onChangePage(0);
                              }, (state.currentPage == 0)),
                              buildRow("Sign In", () {
                                _cubit?.onChangePage(1);
                              }, (state.currentPage == 1))
                            ],
                          ),
                          Container(
                            height: (state.currentPage == 0) ? 230 : 220,
                            child: PageView(
                              physics: NeverScrollableScrollPhysics(),
                              controller: _cubit?.controller,
                              children: [SignUp(), SignIn()],
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Container(
                          padding: EdgeInsets.all(15),
                          decoration: BoxDecoration(
                            border: Border.all(color: Colors.black),
                            shape: BoxShape.circle,
                          ),
                          child: Image.asset('images/ic_facebook.png'),
                        ),
                        Container(
                          padding: EdgeInsets.all(15),
                          decoration: BoxDecoration(
                            border: Border.all(color: Colors.black),
                            shape: BoxShape.circle,
                          ),
                          child: Image.asset('images/ic_insta.png'),
                        ),
                        Container(
                          padding: EdgeInsets.all(15),
                          decoration: BoxDecoration(
                            border: Border.all(color: Colors.black),
                            shape: BoxShape.circle,
                          ),
                          child: Image.asset('images/ic_insta.png'),
                        ),
                        Container(
                          padding: EdgeInsets.all(13),
                          decoration: BoxDecoration(
                            border: Border.all(color: Colors.black),
                            shape: BoxShape.circle,
                          ),
                          child: Image.asset('images/face_id.png'),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'By signing in you agree to our ',
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 15),
                        ),
                        Text(
                          'Term of use',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 15,
                              decoration: TextDecoration.underline),
                        ),
                      ],
                    )
                  ],
                );
              }),
            )
          ],
        ),
      ),
    ));
  }

  Widget buildRow(@required String title, VoidCallback onTap, bool selected) {
    return Expanded(
      child: GestureDetector(
        onTap: onTap,
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 25),
          decoration: BoxDecoration(
              border: Border(
                  bottom: BorderSide(
                      width: (selected) ? 3 : 1,
                      color:
                          (selected) ? Color(0xFF109F2E) : Color(0xFF34495E)))),
          alignment: Alignment.center,
          child: Text(
            title,
            style: TextStyle(
                color: (selected) ? Color(0xFF109F2E) : Color(0xFF34495E),
                fontSize: 20,
                fontWeight: FontWeight.bold),
          ),
        ),
      ),
    );
  }
}
