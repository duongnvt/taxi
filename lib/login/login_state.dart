part of 'login_cubit.dart';

class LoginState {
  final int? currentPage;
  LoginState({
    this.currentPage,
  });

  LoginState copyWith({
    int? currentPage,
  }) {
    return LoginState(
      currentPage: currentPage ?? this.currentPage,
    );
  }
}
