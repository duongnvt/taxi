import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';

part 'login_state.dart';

class LoginCubit extends Cubit<LoginState> {
  PageController controller = PageController();
  LoginCubit() : super(LoginState());
  void Page(){
    controller = PageController(initialPage: 0);
    emit(state.copyWith(currentPage: 0));
  }
  void onChangePage(int index){
    controller.jumpToPage(index);
    emit(state.copyWith(currentPage: index));
  }
}
