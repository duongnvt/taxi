import 'package:country_code_picker/country_code_picker.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:taxi/login/login_cubit.dart';
import 'package:taxi/otp/otp_page.dart';

class SignIn extends StatefulWidget {
  const SignIn({Key? key}) : super(key: key);

  @override
  State<SignIn> createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  final GlobalKey<ScaffoldState> _scaffoldkey = GlobalKey<ScaffoldState>();
  FirebaseAuth _auth = FirebaseAuth.instance;
  String? countrycode;
  TextEditingController _controller = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldkey,
      body: Column(
        children: [
          Container(
            padding: EdgeInsets.fromLTRB(20, 30, 20, 0),
            alignment: Alignment.center,
            child: Column(
              children: [
                Text(
                  'Login with your Phone Number',
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                ),
                SizedBox(height: 20,),
                Row(
                  children: [
                    Expanded(
                      child: Container(
                        decoration: BoxDecoration(
                          border: Border.all(color: Color(0xFF34495E)),
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: CountryCodePicker(
                          alignLeft: true,
                          onInit: (country){
                            countrycode = country?.dialCode!;
                          },
                          onChanged: (country) {
                            countrycode = country.dialCode!;
                          },
                          flagWidth: 25,
                          initialSelection: '+84',
                          showOnlyCountryWhenClosed: false,
                          favorite: ['+84'],
                        ),
                      ),
                    ),
                    SizedBox(width: 10,),
                    Expanded(
                      child: Container(
                        decoration: BoxDecoration(
                          border: Border.all(color: Color(0xFF34495E)),
                          borderRadius: BorderRadius.circular(10),
                        ),
                        padding: EdgeInsets.symmetric(horizontal: 15),
                        child: TextField(
                          controller: _controller,
                          inputFormatters: [
                            FilteringTextInputFormatter.digitsOnly,
                            LengthLimitingTextInputFormatter(11),
                          ],
                          keyboardType: TextInputType.number,
                          maxLines: 1,
                          decoration: InputDecoration(
                            border: InputBorder.none,
                          ),
                          style: TextStyle(fontSize: 14),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 30,),
                BlocBuilder<LoginCubit, LoginState>(
                  builder: (context, state) {
                    return GestureDetector(
                      onTap: () async {
                        await _auth.verifyPhoneNumber(
                            phoneNumber: countrycode!+_controller.text,
                            verificationCompleted: (phoneAuthCredential) async {},
                            verificationFailed: (verificationFailed) async{
                            },
                            codeSent: (verificationID, resendingToken) async {
                              Navigator.of(context).push(MaterialPageRoute(builder: (
                                  c) =>
                                  OTPScreen(
                                    countrycode: countrycode,
                                    phone: _controller.text,
                                    verificationID: verificationID,
                                  )));
                            },
                            codeAutoRetrievalTimeout: (verificationID) async {}
                            );
                      },
                      child: Container(
                        alignment: Alignment.center,
                        width: double.infinity,
                        padding: EdgeInsets.symmetric(vertical: 15),
                        decoration: BoxDecoration(
                            boxShadow: [
                              BoxShadow(
                                color: Color(0xFF51C569),
                                blurRadius: 15.0,
                                offset: Offset(2.0, 2.0),
                              )
                            ],
                            borderRadius: BorderRadius.circular(10),
                            gradient: LinearGradient(
                                begin: Alignment.centerRight,
                                end: Alignment.centerLeft,
                                colors: [
                                  Color(0xFF51C569),
                                  Color(0xFF109F2E),
                                ])),
                        child: Text(
                          'SIGN IN',
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    );
                  },
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
