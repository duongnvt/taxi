import 'package:flutter/material.dart';

class ContactScreen extends StatefulWidget {
  const ContactScreen({Key? key}) : super(key: key);

  @override
  State<ContactScreen> createState() => _ContactScreenState();
}

class _ContactScreenState extends State<ContactScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: true,
        centerTitle: true,
        leading: GestureDetector(
          onTap: (){
            Navigator.pop(context);
          },
            child: Image.asset('images/ic_back.png')),
        title: Text('CONTACTS'),
        bottom: PreferredSize(
          preferredSize: Size.fromHeight(70),
          child: Container(
            margin: EdgeInsets.fromLTRB(15, 0, 15, 20),
            padding: EdgeInsets.symmetric(horizontal: 10),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
            ),
            child: Row(
              children: [
                Container(
                    alignment:Alignment.center,
                    child: Icon(Icons.search,)),
                SizedBox(width: 10,),
                Expanded(
                  child: Container(
                    child: TextField(
                      maxLines: 1,
                      decoration: InputDecoration(
                        hintText: 'Search',
                        border: InputBorder.none,
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
      body: Container(
        margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
        child: ListView(
          children: [
            _buildRow('Adrianne', 'Now'),
            _buildRow("Ivone", "12:15"),
          ],
        ),
      ),
    );
  }
  Widget _buildRow(@required String name, @required String time){
    return Container(
      padding: EdgeInsets.symmetric(vertical: 15),
      decoration: BoxDecoration(
          border: Border(
              bottom: BorderSide(width: 1,color: Colors.grey)
          )
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              Image.asset('images/circle_avatar.png'),
              SizedBox(width: 15,),
              Text(name,style: TextStyle(fontWeight: FontWeight.bold,fontSize: 14),),
            ],
          ),
          Text(time,style: TextStyle(color: Color(0xFF2BAF46),fontSize: 14),)
        ],
      ),
    );
  }
}
